// create the module and name it scotchApp
    // also include ngRoute for all our routing needs
var Basic = angular.module('Basic', ['ngRoute']);

// configure our routes
Basic.config(function($routeProvider, $locationProvider) {
    $routeProvider

        // route for the home page
        .when('/', {
            templateUrl : 'pages/home.html',
            controller  : 'mainController'
        })

        // route for the about page
        .when('/about', {
            templateUrl : 'pages/about.html',
            controller  : 'aboutController'
        })

        // route for the contact page
        .when('/contact', {
            templateUrl : 'pages/contact.html',
            controller  : 'contactController'
        });
        $locationProvider.html5Mode(true);
});
